cls
@echo off

del MiddlewareClient.class
del MiddlewareServer.class

javac MiddlewareClient.java
javac MiddlewareServer.java

start "Server" call ServerRun.bat
start "Client 1" call ClientRun.bat
timeout 1
start "Client 2" call ClientRun.bat