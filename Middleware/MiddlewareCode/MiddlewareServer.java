import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class MiddlewareServer
{

	public static void main(String[] args)
	{
		try
		{
			ServerSocket sock = new ServerSocket(50123);
			int syscheck = 0;

			while (true)
			{
				if ((syscheck % 2) == 0)
					System.out.println("Server ready...");
					
				Socket client = sock.accept();
				PrintWriter put = new PrintWriter(client.getOutputStream(), true);

				put.println(new java.util.Date().toString());
				client.close();
				syscheck++;
			}
		} catch (IOException ioe)
		{
			System.err.println(ioe);
		}
	}
}
