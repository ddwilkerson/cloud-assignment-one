import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
	private Client() {}
	public static void main(String[] args)
	{
		String IPaddr = "10.0.2.15"; //Local IP = 127.0.0.1; Ubuntu IP = 10.0.2.15
		int port = 50123;
		
		try {
			Registry registry = LocateRegistry.getRegistry(IPaddr, port);
			//Registry registry = LocateRegistry.getRegistry(null);
			Hello stub = (Hello) registry.lookup("Hello");
			stub.printMsg();
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}