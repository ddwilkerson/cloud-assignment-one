*IMPORTANT*
rmiregistry needs to be installed on the computer before it will run
the server code.

*RMI Blocking Port*
1) If rmiregistry is blocking a port run this code on linux to open the port again:
$ sudo netstat -lnp > netstat.txt
2) find the PID of the open port in netstat.txt and use:
$ kill (PID)

How to run:
Execute the build.bat file and everything else is handled.
